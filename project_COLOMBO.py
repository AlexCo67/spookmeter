import time
from umqtt.simple import MQTTClient
import ujson
import machine
from machine import Pin,PWM, I2C, ADC
from sgp40 import SGP40
import dht
import esp32
import ufirebase as firebase
import urequests


#DHT SETTINGS
d = dht.DHT11(Pin(25))

#LED SETTINGS
ledR = Pin(21, Pin.OUT)
ledG = Pin(23, Pin.OUT)
ledB = Pin(22, Pin.OUT)

currentLedTracking="Temperature"

#LIGHT SETTINGS
foto=ADC(Pin(34))
foto.atten(ADC.ATTN_11DB)

#BUZZER SETTINGS
buzzer_pin = machine.Pin(17, machine.Pin.OUT)

#SONG AND KEYS
C6  = 1047
CS6 = 1109
D6  = 1175
DS6 = 1245
E6  = 1319
F6  = 1397
FS6 = 1480
G6  = 1568
GS6 = 1661
A6  = 1760
AS6 = 1865
B6  = 1976
C7  = 2093
CS7 = 2217
D7  = 2349
DS7 = 2489
E7  = 2637
F7  = 2794
FS7 = 2960
G7  = 3136
GS7 = 3322
A7  = 3520
AS7 = 3729
B7  = 3951

song=[
    D6,0,D6,0,D7,0,0,A6
    ]


buzzerIsAlert=False

def play(buz_pin, notes, delay, active_duty=50):
    buz = machine.PWM(buz_pin)
    for note in notes:
        if note == 0:  # Special case for silence
            buz.duty(0)
        else:
            buz.freq(note)
            buz.duty(active_duty)
        time.sleep(delay)
    buz.duty(0)
    buz.deinit()
    
def alert(buz_pin):
    buz = machine.PWM(buz_pin)
    buz.duty(256)
    buz.freq(1000)
    
def stopBuzz(buz_pin):
    buz = machine.PWM(buz_pin)
    buz.deinit()

#DATABASE SETTINGS
URL="https://esp-32-colombo-default-rtdb.europe-west1.firebasedatabase.app/"
firebase.setURL(URL)
THINKSPEAK_WRITE_API_KEY='UFXWRJ0LRKUEV0MD'
HTTP_HEADERS={'Content-Type':'application/json'}
#APPART
#SERVER ='192.168.1.8'
#HOME
SERVER ='192.168.0.36'
CLIENT_ID = 'ESP32_alexa'

def do_connect():
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect('Rema', '31894690')
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())
    
    
def sub_callback(topic, msg):
    print((topic, msg))
    global currentLedTracking
    if topic.decode() == "SpookMeter/LedTracking":
        currentLedTracking=msg.decode()


#VARIABLES SETTINGS FOR PHONE APPLICATION
minusTwoTime=None
minusOneTime=None
measureTime=None

minusTwoTemperature=None
minusOneTemperature=None
currentTemperature=None

minusTwoHumidity=None
minusOneHumidity=None
currentHumidity=None

minusTwoLight=None
minusOneLight=None
currentLight=None

minusTwoEMF=None
minusOneEMF=None
currentEMF=None





#MAIN SCRIPT
#CONNECTION
do_connect()
print("set client")
client = MQTTClient(CLIENT_ID, SERVER, 1883)
print("set callback")
client.set_callback(sub_callback)
print("connect ?")
client.connect()
print("connect ok")

#TOPIC SUBSCRIPTION
TOPIC_SUB = 'SpookMeter/LedTracking'
client.subscribe(TOPIC_SUB, qos=1)
print('Connected to MQTT broker')

try:
    client.publish(topic="SpookMeter/LedTracking", msg=b"Temperature")
except:
    print('Failed to send initial tacking settings')


#WAKE UP SONG
play(buzzer_pin, song, 0.08)


#DATA MEASURE LOOP
while True:
    for i in range(31):
        
        #MEASURE SETTINGS
        
        minusTwoTime=minusOneTime
        minusOneTime=measureTime
        measureTime=time.time()
        
        d.measure()
        minusTwoTemperature=minusOneTemperature
        minusOneTemperature=currentTemperature
        currentTemperature=d.temperature()
        
        minusTwoHumidity=minusOneHumidity
        minusOneHumidity=currentHumidity
        currentHumidity=d.humidity()
        
        minusTwoLight=minusOneLight
        minusOneLight=currentLight
        currentLight=foto.read_u16()
        
        minusTwoEMF=minusOneEMF
        minusOneEMF=currentEMF
        currentEMF=esp32.hall_sensor()
        
        #BUZZER ALERT
        if currentTemperature<10 or currentHumidity>80 or currentEMF <=10:
            alert(buzzer_pin)
            buzzerIsAlert=True
        
        #LED UPDATE
        ledR.value(0)
        ledG.value(0)
        ledB.value(0)
        try:
            client.check_msg()
        except:
            print("failed to receive message")
        if currentLedTracking=="Temperature":
            if currentTemperature>=30:
                ledR.value(1)
            elif currentTemperature>=20:
                ledR.value(1)
                ledG.value(1)
            elif currentTemperature>=10:
                ledG.value(1)
            elif currentTemperature>=5:
                ledG.value(1)
                ledB.value(1)
            elif currentTemperature>=0:
                ledB.value(1)
            elif currentTemperature<0:
                ledG.value(1)
                ledB.value(1)
                ledR.value(1)
        elif currentLedTracking=="Humidity":
            if currentHumidity>=80:
                ledB.value(1)
            elif currentHumidity>=60:
                ledB.value(1)
                ledG.value(1)
            elif currentHumidity>=50:
                ledG.value(1)
            elif currentHumidity>=40:
                ledG.value(1)
                ledR.value(1)
            elif currentHumidity<40:
                ledR.value(1)
                
        elif currentLedTracking=="Light":
            if currentLight>=50000:
                ledR.value(1)
            elif currentLight>=40000:
                ledR.value(1)
                ledG.value(1)
            elif currentLight>=30000:
                ledG.value(1)
            elif currentLight>=20000:
                ledG.value(1)
                ledB.value(1)
            elif currentLight>=10000:
                ledB.value(1)
            elif currentLight<10000:
                ledB.value(1)
                ledR.value(1)
                ledG.value(1)

        elif currentLedTracking=="EMF":
            if currentEMF>=25:
                ledR.value(1)
            elif currentEMF>=20:
                ledR.value(1)
                ledG.value(1)
            elif currentEMF>=15:
                ledG.value(1)
            elif currentEMF>=10:
                ledG.value(1)
                ledB.value(1)
            elif currentEMF>=5:
                ledB.value(1)
            elif currentEMF>=0:
                ledB.value(1)
                ledR.value(1)
            elif currentEMF<0:
                ledB.value(1)
                ledR.value(1)
                ledG.value(1)
                
        try:
            if currentLedTracking=="Temperature":
               #msg={"value":currentTemperature}
                concat=str(currentTemperature)+'C'
            elif currentLedTracking=="Humidity":
                #msg={"value":currentHumidity}
                concat=str(currentHumidity)+'%'
            elif currentLedTracking=="Light":
                #msg={"value":currentLight}
                concat=str(currentLight*100//60000)+'%'
            elif currentLedTracking=="EMF":
                #msg={"value":currentEMF}
                concat=str(currentEMF)
            #jsMsg = ujson.dumps(msg)
            #client.publish(topic="SpookMeter/CurrentMeasure", msg=jsMsg)
            client.publish(topic="SpookMeter/CurrentMeasure", msg=concat)                
        except:
            print("Error when publishing current measure")
    
        #PUBLISHING RESULTS
        try:
            msg={"time": [minusTwoTime,minusOneTime,measureTime],"value":[minusTwoHumidity,minusOneHumidity,currentHumidity]}
            jsMsg = ujson.dumps(msg)
            client.publish(topic="SpookMeter/Humidity", msg=jsMsg)
            
            msg={"time": [minusTwoTime,minusOneTime,measureTime],"value":[minusTwoTemperature,minusOneTemperature,currentTemperature]}
            jsMsg = ujson.dumps(msg)            
            client.publish(topic="SpookMeter/Temperature", msg=jsMsg)
            
            msg={"time": [minusTwoTime,minusOneTime,measureTime],"value":[minusTwoLight,minusOneLight,currentLight]}
            jsMsg = ujson.dumps(msg)            
            client.publish(topic="SpookMeter/Light", msg=jsMsg)
            
            msg={"time": [minusTwoTime,minusOneTime,measureTime],"value":[minusTwoEMF,minusOneEMF,currentEMF]}
            jsMsg = ujson.dumps(msg)            
            client.publish(topic="SpookMeter/EMF", msg=jsMsg)
            
        except:
            print("Error when publishing data")
        

        
        if buzzerIsAlert==True:
            stopBuzz(buzzer_pin)
            buzzerIsAlert=False
            
         #1 MINUTE UPLOAD
        if i==0:
            try:
                print("begin data upload")
                measured_values={"field1":currentTemperature, "field2":currentHumidity, "field3":currentLight, "field4":currentEMF}
                request=urequests.post('https://api.thingspeak.com/update?api_key=UFXWRJ0LRKUEV0MD',json = measured_values, headers = HTTP_HEADERS)
                request.close()
                firebase.patch("Measures", {measureTime:{"Temperature":currentTemperature,"Humidity":currentHumidity,"Light":currentLight,"EMF":currentEMF}}, id=0)
                print("end data upload")
            except:
                print("Error when uploading data")
 
 
        time.sleep(2)
    
